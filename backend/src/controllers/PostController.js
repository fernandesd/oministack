const Post = require('../models/Post');
const sharp = require('sharp'); //manipulação de imagens
const path = require('path');
const fs = require('fs');

module.exports = {
	async index(req, res){
		const posts = await Post.find().sort('-createdAt');

		return res.json(posts);
	},

	async store(req, res){
		const { author, place, description, hashtags } = req.body;
		const { filename: image } = req.file;

		const [name] = image.split('.');
		const fileName = `${name}.jpg`;

		await sharp(req.file.path) //req.file.path - tem o caminho onde a imagem foi salva
			.resize(500) //mudando tamanho da imagem para o app
			.jpeg({ quality: 70 }) //qualidade máxima 70%
			.toFile(
				path.resolve(req.file.destination, 'resized', fileName)//req.file.destination - tem o caminho até a pasta uploads
				)

			fs.unlinkSync(req.file.path); //Deleta a imagem original

		const post = await Post.create({
			author,
			place,
			description,
			hashtags,
			image: fileName,
		});

		req.io.emit('post', post);

		return res.json({post});
	}
};